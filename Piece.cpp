#include "Piece.h"

Piece::Piece() : GameActor() {
    this->_totalMove = -1;
    this->_oldTotalMove = -1;
    this->_waitCount = 0;
    this->_scoresGained = 0;
    this->_scoresLost = 0;
    this->_numOfStarts = 0;
}

Piece::Piece(Color color) {
    this->_color = color;
    this->_totalMove = -1;
    this->_oldTotalMove = -1;
    this->_waitCount = 0;
    this->_scoresGained = 0;
    this->_scoresLost = 0;
    this->_numOfStarts = 0;

}

Piece *Piece::clone() {
    return new Piece(this->_color);
}

void Piece::move(int moveCount) {
    // Check if piece has come into play
    if (moveCount >= 0) {
        this->_oldTotalMove = this->_totalMove;
        if (this->_totalMove == -1) {
            this->_totalMove = 0;
        } else {
            this->_totalMove += moveCount;
        }
    }
}

Color Piece::getColor() const {
    return _color;
}

void Piece::setColor(Color color) {
    _color = color;
}

void Piece::onNotify(EventType event) {
    switch (event) {
        case MOVEMENT_ALLOWED:
            break;
        case IDLE:
            break;
        case PIECE_HIT:
            this->_startOver();
            break;
        case SCORE_LOST:
            this->_scoresLost += 1;
            break;
        case SCORE_GAINED:
            this->_scoresGained += 1;
            break;
        case STARTED:
            this->_numOfStarts += 1;
            break;
        case WAITED:
            this->_waitCount += 1;
            break;
        default:
            break;
    }
}

int Piece::getNumber() const {
    return _number;
}

void Piece::setNumber(int number) {
    _number = number;
}

BoardTile *Piece::getCurrentLocation() const {
    return _currentLocation;
}

void Piece::setCurrentLocation(BoardTile *currentLocation) {
    _currentLocation = currentLocation;
}

void Piece::undoMove() {
    this->_totalMove = this->_oldTotalMove;
}

void Piece::_startOver() {
    this->_oldTotalMove = this->_totalMove;
    this->_totalMove = -1;
}
