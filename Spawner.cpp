#include "Spawner.h"

Spawner::Spawner(Piece *prototype) {
    this->_prototype = prototype;
}

Piece *Spawner::spawnActor() {
    return _prototype->clone();
}
