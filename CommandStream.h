#ifndef MENSCH_COMMANDSTREAM_H
#define MENSCH_COMMANDSTREAM_H

#include "Command.h"
#include <queue>

using namespace std;

class CommandStream {
public:
    static CommandStream *getInstance();

    virtual void push(Command *command);

    virtual Command *pop();

private:
    CommandStream() {};

    std::queue<Command *> _commands;
};

#endif //MENSCH_COMMANDSTREAM_H
