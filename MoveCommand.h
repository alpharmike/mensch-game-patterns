#ifndef MENSCH_MOVECOMMAND_H
#define MENSCH_MOVECOMMAND_H

#include "Command.h"
#include "GameActor.h"

class MoveCommand : public Command {
public:
    MoveCommand(GameActor *actor, int moveCount);

    void execute() override;

    void undo() override;

private:
    GameActor *_actor;
    int _moveCount;
};

#endif //MENSCH_MOVECOMMAND_H
