#include "Game.h"
#include "ctime"
#include <unistd.h>
#include <chrono>

Game::Game() {
    this->_turns[0] = Color::RED;
    this->_turns[1] = Color::BLUE;
    this->_turns[2] = Color::GREEN;
    this->_turns[3] = Color::YELLOW;

    this->_init();
}

void Game::_init() {
    srand(time(nullptr));
    this->_AIEngine = AIEngine::getInstance(this);
    this->_commandStream = CommandStream::getInstance();
    this->_physicsEngine = PhysicsEngine::getInstance();
    this->_renderEngine = RenderEngine::getInstance();
    this->_diagnosticsEngine = DiagnosticsEngine::getInstance(this);
    this->_board = new Board();
    this->_turn = rand() % 4;
    this->_round = 1;
    this->_winnerColor = Color::NONE;
    this->_gameEnded = false;
    this->_startTime = std::chrono::steady_clock::now();
}

void Game::mainLoop() {
    while (!_gameEnded) {
        AICommandInfo commandInfo = this->_AIEngine->generateCommand(_board, this->_turns[_turn]);

        this->_commandStream->push(commandInfo.command);

        while (Command *currCommand = _commandStream->pop()) {
            this->_physicsEngine->executeCommand(currCommand);
        }
        this->_physicsEngine->updateEntity(commandInfo.movedPiece);
        this->_physicsEngine->updateScores(commandInfo.movedPiece, commandInfo.hitPiece);
        this->_physicsEngine->notifyAll();
        this->_physicsEngine->checkForEnd(this);
        //  this->_renderEngine->showLatestMove(commandInfo, this->_turns[_turn], this->_round);
        this->_updateTurn(commandInfo.command != nullptr ? commandInfo.diceValue : 0);
        this->_updateRound();
    }

    double finishedTime = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::steady_clock::now() - this->_startTime).count() / 1000000.0;

    // this->_renderEngine->renderGameStatus(this);
    printf("Final Time: %lf\n", finishedTime);
    this->_diagnosticsEngine->storeData(std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::steady_clock::now() - this->_startTime).count() / 1000000.0);
}

void Game::onNotify(EventType event) {
    switch (event) {
        case GAME_FINISHED:
            this->_gameEnded = true;
            this->_setWinner(this->_physicsEngine->getWinner());
        default:
            break;

    }
}

void Game::_updateTurn(int diceValue) {
    if (diceValue != 6) {
        this->_turn = (this->_turn + 1) % 4;
    }
    usleep(10 * 1000);
}

Board *Game::getBoard() const {
    return _board;
}

void Game::_updateRound() {
    this->_round++;
}

void Game::_setWinner(Color color) {
    this->_winnerColor = color;
}

Color Game::getWinnerColor() const {
    return _winnerColor;
}

PhysicsEngine *Game::getPhysicsEngine() const {
    return _physicsEngine;
}

void Game::_restart() {
    delete this->_board;

    this->_board = new Board();
    this->_turn = rand() % 4;
    this->_round = 1;
    this->_winnerColor = Color::NONE;
    this->_gameEnded = false;
    // this->_startTime = std::chrono::steady_clock::now();
}

void Game::run() {
    for (int gameRound = 0; gameRound < 200; ++gameRound) {
        this->mainLoop();
        this->_restart();
    }
    this->_diagnosticsEngine->saveIncrementalData();
    this->_diagnosticsEngine->plotIncrementalCharts();

    this->_diagnosticsEngine->calculateIntervalData(60.0);
    this->_diagnosticsEngine->plotIntervalCharts();

}
