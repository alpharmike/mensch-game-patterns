#ifndef MENSCH_DIAGNOSTICSENGINE_H
#define MENSCH_DIAGNOSTICSENGINE_H

#include <vector>
#include <unordered_map>
#include "ctime"
#include "Piece.h"

class Game;

using namespace std;

class DiagnosticsEngine {
public:
    static DiagnosticsEngine *getInstance(Game *game);

    void analyzeAll(double currTime);

    void saveIncrementalData();

    void saveIntervalData();

    void storeData(double currTime);

    void calculateIntervalData(double interval);

    void plotIncrementalCharts();

    void plotIntervalCharts();

protected:
    Game *game;

private:
    // for window length
    float _interval;

    std::vector<double> _times;
    std::unordered_map<Color, std::vector<int>> _waitCounts;
    std::unordered_map<Color, std::vector<int>> _scoresGained;
    std::unordered_map<Color, std::vector<int>> _scoresLost;
    std::unordered_map<Color, std::vector<int>> _startsCount;
    std::unordered_map<Color, std::vector<int>> _totalMoveDistance;

    std::unordered_map<Color, std::vector<int>> _waitCountsIncremental;
    std::unordered_map<Color, std::vector<int>> _scoresGainedIncremental;
    std::unordered_map<Color, std::vector<int>> _scoresLostIncremental;
    std::unordered_map<Color, std::vector<int>> _startsCountIncremental;
    std::unordered_map<Color, std::vector<int>> _totalMoveDistanceIncremental;

    std::vector<double> _intervalTimes;
    std::unordered_map<Color, std::vector<int>> _waitCountsInterval;
    std::unordered_map<Color, std::vector<int>> _scoresGainedInterval;
    std::unordered_map<Color, std::vector<int>> _scoresLostInterval;
    std::unordered_map<Color, std::vector<int>> _startsCountInterval;
    std::unordered_map<Color, std::vector<int>> _totalMoveDistanceInterval;


    void _initCriteria();

    void _analyzeWaitCounts();

    std::unordered_map<Color, int> _initPieceRecords();

    void _analyzeScoresGained();

    void _analyzeScoresLost();

    void _analyzeStartsCount();

    void _analyzeTotalMoveDistance();

    void _updateIncrementalData();


    void _loadData();

    void _plotCharts(std::vector<double> x, std::vector<std::vector<int>> y, string title, std::vector<string> names, std::vector<string> colors, bool interval);


};

#endif //MENSCH_DIAGNOSTICSENGINE_H
