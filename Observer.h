#ifndef MENSCH_OBSERVER_H
#define MENSCH_OBSERVER_H

#include "enums/EventType.h"

class Observer {
public:

    virtual void onNotify(EventType eventType) = 0;

};

#endif //MENSCH_OBSERVER_H
