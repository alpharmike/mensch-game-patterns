#ifndef MENSCH_PIECE_H
#define MENSCH_PIECE_H

#include "GameActor.h"
#include "enums/Color.h"
#include "BoardTile.h"

class Piece : public GameActor {
public:
    Piece();

    explicit Piece(Color color);

    void move(int moveCount) override;

    Piece *clone();

    Color getColor() const;

    void setColor(Color color);


    void onNotify(EventType event) override;

    int getNumber() const;

    void setNumber(int number);

    BoardTile *getCurrentLocation() const;

    void setCurrentLocation(BoardTile *currentLocation);

    void undoMove() override;


private:
    Color _color;
    int _number;
    BoardTile* _currentLocation;

    void _startOver();
};

#endif //MENSCH_PIECE_H
