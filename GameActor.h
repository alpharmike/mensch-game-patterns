#ifndef MENSCH_GAMEACTOR_H
#define MENSCH_GAMEACTOR_H

#include "Observer.h"

class GameActor : public Observer {
public:

    virtual void move(int moveCount) = 0;

    virtual void undoMove() = 0;

    void onNotify(EventType event) override = 0;

    int getTotalMove() const;

    void setTotalMove(int totalMove);

    int getOldTotalMove() const;

    void setOldTotalMove(int oldTotalMove);

    int getWaitCount() const;

    void setWaitCount(int waitCount);

    int getScoresGained() const;

    void setScoresGained(int scoresGained);

    int getScoresLost() const;

    void setScoresLost(int scoresLost);

    int getNumOfStarts() const;

    void setNumOfStarts(int numOfStarts);

protected:
    int _totalMove;
    int _oldTotalMove;
    int _waitCount;
    int _scoresGained;
    int _scoresLost;
    int _numOfStarts;

};

#endif //MENSCH_GAMEACTOR_H
