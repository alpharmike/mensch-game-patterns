#ifndef MENSCH_BOARD_H
#define MENSCH_BOARD_H

#define TILE_COUNT 40
#define SINGLE_PIECE_NUM 4

#include <vector>
#include <unordered_map>
#include "BoardTile.h"
#include "GameActor.h"
#include "Piece.h"
#include "Spawner.h"
#include "Dice.h"

using namespace std;

class Board {
public:
    Board();

    ~Board() {};

    int getHomeTile(Color homeColor);

    const unordered_map<int, BoardTile *> &getHomeTiles() const;

    void setHomeTiles();

    Dice *getDice() const;

    const vector<Piece *> &getPieces() const;

    const vector<Color> &getHomeColors() const;



private:
    std::vector<BoardTile *> _tiles;
    std::vector<Piece *> _pieces;
    std::unordered_map<int, BoardTile *> _homeTiles;
    std::vector<Color> _homeColors;


    BoardTile _redHome;
    BoardTile _yellowHome;
    BoardTile _blueHome;
    BoardTile _greenHome;
    BoardTile _ordinaryTile;

    Dice *_dice;
    Spawner *_pieceSpawner;

    void _init();

    void _initTiles();

    void _initPieces();

    void _initHomeColors();
};


#endif //MENSCH_BOARD_H
