#include "CommandStream.h"

CommandStream *CommandStream::getInstance() {
    static auto *instance = new CommandStream();
    return instance;
}

void CommandStream::push(Command *command) {
    _commands.push(command);
}

Command *CommandStream::pop() {
    if (_commands.empty()) return nullptr;

    auto command = _commands.front();
    _commands.pop();

    return command;
}