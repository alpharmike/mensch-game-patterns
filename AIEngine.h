#ifndef MENSCH_AIENGINE_H
#define MENSCH_AIENGINE_H

#include "Dice.h"
#include "Command.h"
#include "Board.h"
#include "AICommandInfo.h"

class Game;

#define MAX_MOVEMENT 43
#define HOME_ROW_START 40

class AIEngine {
public:
    static AIEngine *getInstance(Game* game);

    AICommandInfo generateCommand(Board* board, Color pieceColor);

    int selectRandomPiece(std::vector<int> chosenNumbers);

protected:
    Game* game;

private:
    AIEngine() {};

    AICommandInfo validateMove(Board* board, Piece* piece, int newTotalMove);


};

#endif //MENSCH_AIENGINE_H
