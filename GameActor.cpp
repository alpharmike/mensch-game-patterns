#include "GameActor.h"

int GameActor::getTotalMove() const {
    return _totalMove;
}

void GameActor::setTotalMove(int totalMove) {
    _totalMove = totalMove;
}

int GameActor::getOldTotalMove() const {
    return _oldTotalMove;
}

void GameActor::setOldTotalMove(int oldTotalMove) {
    _oldTotalMove = oldTotalMove;
}

int GameActor::getWaitCount() const {
    return _waitCount;
}

void GameActor::setWaitCount(int waitCount) {
    _waitCount = waitCount;
}

int GameActor::getScoresGained() const {
    return _scoresGained;
}

void GameActor::setScoresGained(int scoresGained) {
    _scoresGained = scoresGained;
}

int GameActor::getScoresLost() const {
    return _scoresLost;
}

void GameActor::setScoresLost(int scoresLost) {
    _scoresLost = scoresLost;
}

int GameActor::getNumOfStarts() const {
    return _numOfStarts;
}

void GameActor::setNumOfStarts(int numOfStarts) {
    _numOfStarts = numOfStarts;
}
