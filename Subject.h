#ifndef MENSCH_SUBJECT_H
#define MENSCH_SUBJECT_H

#include "Observer.h"
#include "GameActor.h"
#include <vector>
#include <unordered_map>

using namespace std;

class Subject
{
public:

    void unsubscribe(Observer *observer, EventType eventType);

    void subscribe(Observer* observer, EventType eventType);

    void notify(EventType eventType);

    void notifyAll();

private:
    std::unordered_map<EventType, std::vector<Observer*>> _observers;
};

#endif //MENSCH_SUBJECT_H
