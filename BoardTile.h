#ifndef MENSCH_BOARDTILE_H
#define MENSCH_BOARDTILE_H

#include "enums/Color.h"
#include "enums/TileType.h"

class BoardTile {
public:
    BoardTile(Color color, TileType tileType);

    void setColor(Color color);

    void setTileType(TileType tileType);

    Color getColor() const;

    TileType getTileType() const;

private:
    Color _color;
    TileType _tileType;

};

#endif //MENSCH_BOARDTILE_H
