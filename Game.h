#ifndef MENSCH_GAME_H
#define MENSCH_GAME_H

#include "CommandStream.h"
#include "AIEngine.h"
#include "PhysicsEngine.h"
#include "RenderEngine.h"
#include "DiagnosticsEngine.h"
#include <chrono>
#include <unordered_map>

using namespace std;

class Game : public Observer {
public:
    Game();

    virtual void mainLoop();


    void onNotify(EventType event) override;

    Board *getBoard() const;

    Color getWinnerColor() const;

    PhysicsEngine *getPhysicsEngine() const;

    void run();


private:
    CommandStream *_commandStream;
    AIEngine *_AIEngine;
    PhysicsEngine *_physicsEngine;
    RenderEngine *_renderEngine;
    DiagnosticsEngine *_diagnosticsEngine;
    Board *_board;
    Color _winnerColor;
    std::unordered_map<int, Color> _turns;
    int _turn;
    int _round;
    bool _gameEnded;

    std::chrono::steady_clock::time_point _startTime;

    void _init();
    void _updateTurn(int diceValue);
    void _updateRound();
    void _setWinner(Color color);
    void _restart();
//    void _simpleMode();
//    void _runDiagnosticsMode();

};

#endif //MENSCH_GAME_H
