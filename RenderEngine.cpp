#include "RenderEngine.h"
#include "Game.h"


RenderEngine *RenderEngine::getInstance() {
    static auto *instance = new RenderEngine();
    return instance;
}

void RenderEngine::renderGameStatus(Game *game) {
    if (this->_colorTranslations.empty()) {
        this->_initColorTranslations();
    }

    for (Piece *piece : game->getBoard()->getPieces()) {
        // Indicates that the piece has not started yet
        if (piece->getTotalMove() < 0) {
            printf("Piece %d of Color %s has not started to move yet.\n", piece->getNumber(),
                   this->_colorTranslations[piece->getColor()]);
        } else {
            printf("Piece %d of Color %s has passed %d tiles.\n", piece->getNumber(),
                   this->_colorTranslations[piece->getColor()], piece->getTotalMove());
        }
    }

    if (game->getWinnerColor() != Color::NONE) {
        printf("Color %s won the game!", this->_colorTranslations[game->getWinnerColor()]);
    }
}


void RenderEngine::_initColorTranslations() {
    this->_colorTranslations[Color::RED] = "Red";
    this->_colorTranslations[Color::BLUE] = "Blue";
    this->_colorTranslations[Color::GREEN] = "Green";
    this->_colorTranslations[Color::YELLOW] = "Yellow";
}

void RenderEngine::showLatestMove(AICommandInfo commandInfo, Color turn, int round) {
    if (this->_colorTranslations.empty()) {
        this->_initColorTranslations();
    }
    printf("Round: %d\n", round);
    printf("Turn: %s\n", this->_colorTranslations[turn]);
    printf("Dice: %d\n", commandInfo.diceValue);
    if (commandInfo.movedPiece == nullptr) {
        printf("No movement took place.\n");
    } else {
        printf("Piece %d of Color %s moved %d tiles\n", commandInfo.movedPiece->getNumber(),
               this->_colorTranslations[commandInfo.movedPiece->getColor()], commandInfo.diceValue);
        if (commandInfo.hitPiece != nullptr) {
            printf("Piece %d of color %s with total move of %d was hit\n", commandInfo.hitPiece->getNumber(),
                   this->_colorTranslations[commandInfo.hitPiece->getColor()], commandInfo.hitPiece->getOldTotalMove());
        }
        printf("Number of starts for this piece: %d\n", commandInfo.movedPiece->getNumOfStarts());
        printf("Number of waits for this piece: %d\n", commandInfo.movedPiece->getWaitCount());
        printf("Number of scores gained by this piece: %d\n", commandInfo.movedPiece->getScoresGained());
        printf("Number of scores lost by this piece: %d\n", commandInfo.movedPiece->getScoresLost());
        printf("Total move of this piece: %d\n", commandInfo.movedPiece->getTotalMove());
    }

    printf("----------------------------------------\n");
}

