#include <iostream>
#include "AIEngine.h"
#include "MoveCommand.h"
#include <algorithm>
#include "Game.h"

#define stringify( name ) # name

using namespace std;

AIEngine *AIEngine::getInstance(Game* game) {
    static auto *instance = new AIEngine();
    instance->game = game;
    return instance;
}

AICommandInfo AIEngine::generateCommand(Board *board, Color pieceColor) {
    int diceValue = board->getDice()->roll();
    MoveCommand* moveCommand;
    std::vector<int> randomPieceNumbers;
    int randomPieceNumber = selectRandomPiece(randomPieceNumbers);
    randomPieceNumbers.push_back(randomPieceNumber);
    AICommandInfo commandInfo;

    for (Piece *piece : board->getPieces()) {
        if (piece->getColor() == pieceColor && piece->getNumber() == randomPieceNumber) {
            // check if piece has not started yet
            if (piece->getTotalMove() == -1) {
                // check if 6 has come
                if (diceValue == 6 && (commandInfo = validateMove(board, piece, 0)).isValid) {
                    moveCommand = new MoveCommand(piece, 0);
                    return {moveCommand, diceValue, piece, commandInfo.hitPiece, commandInfo.isValid};
                } else {
                    // to later notify the piece to increment its total waits
                    this->game->getPhysicsEngine()->subscribe(piece, WAITED);
                }
                randomPieceNumber = selectRandomPiece(randomPieceNumbers);
                // when none of the pieces of that color can move
                if (randomPieceNumber < 0) {
                    break;
                }
                randomPieceNumbers.push_back(randomPieceNumber);


            } else {
                int newTotalMove = piece->getTotalMove() + diceValue;
                if ((commandInfo = validateMove(board, piece, newTotalMove)).isValid) {
                    moveCommand = new MoveCommand(piece, diceValue);
                    return {moveCommand, diceValue, piece, commandInfo.hitPiece, commandInfo.isValid};
                } else {
                    this->game->getPhysicsEngine()->subscribe(piece, WAITED);
                }
                randomPieceNumber = selectRandomPiece(randomPieceNumbers);
                // when none of the pieces of that color can move
                if (randomPieceNumber < 0) {
                    break;
                }
                randomPieceNumbers.push_back(randomPieceNumber);

            }
        }
    }

    // no movement
    return {nullptr, diceValue, nullptr, nullptr, true};
}

AICommandInfo AIEngine::validateMove(Board *board, Piece *piece, int newTotalMove) {
    // check if moving more than allowed
    if (newTotalMove > MAX_MOVEMENT) {
        return {piece, nullptr, false};
    }

    for (Piece *currPiece : board->getPieces()) {
        if (newTotalMove >= HOME_ROW_START) {
            if (piece != currPiece && piece->getColor() == currPiece->getColor() && currPiece->getTotalMove() == newTotalMove) {
                return {piece, nullptr, false};;
            }
        } else if (piece != currPiece && piece->getColor() != currPiece->getColor() &&
            newTotalMove < HOME_ROW_START && currPiece->getTotalMove() < HOME_ROW_START && currPiece->getTotalMove() >= 0 && newTotalMove >= 0) {
            if (board->getHomeTile(piece->getColor()) - board->getHomeTile(currPiece->getColor()) == currPiece->getTotalMove() - newTotalMove) {
                return {piece, currPiece, true};;
            } else {
                return {piece, nullptr, true};;
            }
        }
    }
    return {piece, nullptr, true};;
}

int AIEngine::selectRandomPiece(std::vector<int> chosenNumbers) {
    if (chosenNumbers.size() >= 4) {
        return -1;
    }
    int newRandom;
    while (std::find(chosenNumbers.begin(), chosenNumbers.end(), (newRandom = (rand() % 4) + 1)) != chosenNumbers.end());
    return newRandom;
}
