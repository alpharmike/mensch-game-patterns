#include "MoveCommand.h"

MoveCommand::MoveCommand(GameActor *actor, int moveCount) {
    this->_actor = actor;
    this->_moveCount = moveCount;
}

void MoveCommand::execute() {
    this->_actor->move(_moveCount);
}

void MoveCommand::undo() {
    // return to the old moveCount (Saved in the actor)
    this->_actor->undoMove();

}

