#include "Subject.h"


void Subject::notify(EventType eventType) {
    std::vector<Observer*> observers = this->_observers[eventType];
    for (auto & observer : observers) {
        observer->onNotify(eventType);
        this->unsubscribe(observer, eventType);
    }
}

void Subject::subscribe(Observer *observer, EventType eventType) {
    if (observer != nullptr) {
        if (this->_observers.find(eventType) == this->_observers.end()) {
            this->_observers[eventType] = std::vector<Observer*>();
        }
        this->_observers[eventType].push_back(observer);
    }
}

void Subject::unsubscribe(Observer *observer, EventType eventType) {
    for (auto it = _observers[eventType].begin(); it != _observers[eventType].end(); it++) {
        if (*it == observer) {
            this->_observers[eventType].erase(it);
            return;
        }
    }
}

void Subject::notifyAll() {
    this->notify(STARTED);
    this->notify(WAITED);
    this->notify(PIECE_HIT);
    this->notify(SCORE_GAINED);
    this->notify(SCORE_LOST);
}
