#include "BoardTile.h"

BoardTile::BoardTile(Color color, TileType tileType) {
    this->_color = color;
    this->_tileType = tileType;
}

Color BoardTile::getColor() const {
    return _color;
}

TileType BoardTile::getTileType() const {
    return _tileType;
}

void BoardTile::setColor(Color color) {
    _color = color;
}

void BoardTile::setTileType(TileType tileType) {
    _tileType = tileType;
}
