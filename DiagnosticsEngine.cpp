#include "DiagnosticsEngine.h"
#include <iostream>
#include <fstream>
#include <filesystem>
#include "Game.h"
#include <sstream>
#include "headers/matplotlibcpp.h"

namespace plt = matplotlibcpp;

DiagnosticsEngine *DiagnosticsEngine::getInstance(Game *game) {
    static auto *instance = new DiagnosticsEngine();
    instance->game = game;
    return instance;
}

void DiagnosticsEngine::_initCriteria() {
    Board *board = this->game->getBoard();
    for (Color color : board->getHomeColors()) {
        this->_waitCounts[color] = std::vector<int>();
        this->_scoresGained[color] = std::vector<int>();
        this->_scoresLost[color] = std::vector<int>();
        this->_startsCount[color] = std::vector<int>();
        this->_totalMoveDistance[color] = std::vector<int>();

        this->_waitCountsIncremental[color] = std::vector<int>();
        this->_scoresGainedIncremental[color] = std::vector<int>();
        this->_scoresLostIncremental[color] = std::vector<int>();
        this->_startsCountIncremental[color] = std::vector<int>();
        this->_totalMoveDistanceIncremental[color] = std::vector<int>();
    }
}

std::unordered_map<Color, int> DiagnosticsEngine::_initPieceRecords() {
    Board *board = this->game->getBoard();
    std::unordered_map<Color, int> records;
    for (Color pieceColor : board->getHomeColors()) {
        records[pieceColor] = 0;
    }
    return records;
}

void DiagnosticsEngine::_analyzeWaitCounts() {
    Board *board = this->game->getBoard();
    std::unordered_map<Color, int> records = this->_initPieceRecords();
    for (Piece *piece : board->getPieces()) {
        records[piece->getColor()] += piece->getWaitCount();
    }

    for (Color color : board->getHomeColors()) {
        this->_waitCounts[color].push_back(records[color]);
    }
}

void DiagnosticsEngine::_analyzeScoresGained() {
    Board *board = this->game->getBoard();
    std::unordered_map<Color, int> records = this->_initPieceRecords();
    for (Piece *piece : board->getPieces()) {
        records[piece->getColor()] += piece->getScoresGained();
    }
    for (Color color : board->getHomeColors()) {
        this->_scoresGained[color].push_back(records[color]);
    }
}

void DiagnosticsEngine::_analyzeScoresLost() {
    Board *board = this->game->getBoard();
    std::unordered_map<Color, int> records = this->_initPieceRecords();
    for (Piece *piece : board->getPieces()) {
        records[piece->getColor()] += piece->getScoresLost();
    }
    for (Color color : board->getHomeColors()) {
        this->_scoresLost[color].push_back(records[color]);
    }
}

void DiagnosticsEngine::_analyzeStartsCount() {
    Board *board = this->game->getBoard();
    std::unordered_map<Color, int> records = this->_initPieceRecords();
    for (Piece *piece : board->getPieces()) {
        records[piece->getColor()] += piece->getNumOfStarts();
    }
    for (Color color : board->getHomeColors()) {
        this->_startsCount[color].push_back(records[color]);
    }
}

void DiagnosticsEngine::_analyzeTotalMoveDistance() {
    Board *board = this->game->getBoard();
    std::unordered_map<Color, int> records = this->_initPieceRecords();
    for (Piece *piece : board->getPieces()) {
        records[piece->getColor()] += piece->getTotalMove() < 0 ? 0 : piece->getTotalMove();
    }
    for (Color color : board->getHomeColors()) {
        this->_totalMoveDistance[color].push_back(records[color]);
    }
}

void DiagnosticsEngine::analyzeAll(double currTime) {
    this->_times.push_back(currTime);
    this->_analyzeWaitCounts();
    this->_analyzeScoresGained();
    this->_analyzeScoresLost();
    this->_analyzeStartsCount();
    this->_analyzeTotalMoveDistance();
}

void DiagnosticsEngine::saveIncrementalData() {
    ofstream storageFile;
    storageFile.open("incremental_without.txt");
    Board *board = this->game->getBoard();
    if (storageFile.is_open()) {
        for (int index = 0; index < this->_times.size(); ++index) {
            std::ostringstream oss;
            oss << this->_times[index] << " ";
            for (Color pieceColor : board->getHomeColors()) {
                oss << this->_waitCountsIncremental[pieceColor][index] << "/"
                    << this->_scoresGainedIncremental[pieceColor][index] << "/"
                    << this->_scoresLostIncremental[pieceColor][index] << "/"
                    << this->_startsCountIncremental[pieceColor][index] << "/"
                    << this->_totalMoveDistanceIncremental[pieceColor][index] << " ";
            }
            oss << "\n";
            storageFile << oss.str();
        }

        storageFile.close();
    }
}

void DiagnosticsEngine::saveIntervalData() {
    ofstream storageFile;
    storageFile.open("diagnostics_interval.txt");
    Board *board = this->game->getBoard();
    if (storageFile.is_open()) {
        for (int index = 0; index < this->_waitCountsInterval[RED].size(); ++index) {
            std::ostringstream oss;
            oss << this->_times[index] << " ";
            for (Color pieceColor : board->getHomeColors()) {
                oss << this->_waitCountsInterval[pieceColor][index] << "/"
                    << this->_scoresGainedInterval[pieceColor][index] << "/"
                    << this->_scoresLostInterval[pieceColor][index] << "/"
                    << this->_startsCountInterval[pieceColor][index] << "/"
                    << this->_totalMoveDistanceInterval[pieceColor][index] << " ";
            }
            oss << "\n";
            storageFile << oss.str();
        }

        storageFile.close();
    }
}

void DiagnosticsEngine::_updateIncrementalData() {
    Board *board = this->game->getBoard();
    for (Color color : board->getHomeColors()) {
        // first time finding incremental values
        if (this->_times.size() == 1) {
            this->_waitCountsIncremental[color].push_back(this->_waitCounts[color].back());
            this->_scoresGainedIncremental[color].push_back(this->_scoresGained[color].back());
            this->_scoresLostIncremental[color].push_back(this->_scoresLost[color].back());
            this->_startsCountIncremental[color].push_back(this->_startsCount[color].back());
            this->_totalMoveDistanceIncremental[color].push_back(this->_totalMoveDistance[color].back());
        } else {
            this->_waitCountsIncremental[color].push_back(
                    this->_waitCountsIncremental[color].back() + this->_waitCounts[color].back());
            this->_scoresGainedIncremental[color].push_back(
                    this->_scoresGainedIncremental[color].back() + this->_scoresGained[color].back());
            this->_scoresLostIncremental[color].push_back(
                    this->_scoresLostIncremental[color].back() + this->_scoresLost[color].back());
            this->_startsCountIncremental[color].push_back(
                    this->_startsCountIncremental[color].back() + this->_startsCount[color].back());
            this->_totalMoveDistanceIncremental[color].push_back(
                    this->_totalMoveDistanceIncremental[color].back() + this->_totalMoveDistance[color].back());
        }
    }

}

void DiagnosticsEngine::storeData(double currTime) {
    this->analyzeAll(currTime);
    this->_updateIncrementalData();

}

void DiagnosticsEngine::calculateIntervalData(double interval) {
    // calculates interval data using incremental data
    int initIndex = 0;
    int secIndex = initIndex + 1;
    Board *board = this->game->getBoard();
    while (initIndex < this->_times.size()) {
        while (secIndex < this->_times.size()) {
            if (this->_times[secIndex] - this->_times[initIndex] >= interval) {
                for (Color color : board->getHomeColors()) {
                    this->_waitCountsInterval[color].push_back(this->_waitCountsIncremental[color][secIndex] -
                                                               this->_waitCountsIncremental[color][initIndex]);
                    this->_scoresGainedInterval[color].push_back(this->_scoresGainedIncremental[color][secIndex] -
                                                                 this->_scoresGainedIncremental[color][initIndex]);
                    this->_scoresLostInterval[color].push_back(this->_scoresLostIncremental[color][secIndex] -
                                                               this->_scoresLostIncremental[color][initIndex]);
                    this->_startsCountInterval[color].push_back(this->_startsCountIncremental[color][secIndex] -
                                                                this->_startsCountIncremental[color][initIndex]);
                    this->_totalMoveDistanceInterval[color].push_back(
                            this->_totalMoveDistanceIncremental[color][secIndex] -
                            this->_totalMoveDistanceIncremental[color][initIndex]);
                }
                initIndex = secIndex - 1;
                this->_intervalTimes.push_back(this->_times[secIndex]);
                break;
            }
            ++secIndex;
        }
        ++initIndex;
    }
}

void DiagnosticsEngine::_loadData() {
    std::string temp;
    std::string line;
    double d_temp;
    double current_v;
    float roundEndTime;

    std::ifstream fin("diagnostics.txt");
    if (!fin) {
        return;
    }
    while (getline(fin, line)) {
        std::istringstream iss(line);
        iss >> roundEndTime;
        iss.ignore(1);
    }
}

void DiagnosticsEngine::_plotCharts(std::vector<double> x, std::vector<std::vector<int>> y, string title,
                                    std::vector<string> names, std::vector<string> colors, bool interval) {
    // Set the size of output image to 1200x780 pixels
    plt::figure_size(1200, 780);
    // Plot line from given x and y data. Color is selected automatically.

    for (int index = 0; index < y.size(); ++index) {
        plt::named_plot(names[index], x, y[index], colors[index]);
    }

    plt::title(title);
    // Enable legend.
    plt::legend();
    // Save the image (file format is determined by the extension)
    std::ostringstream path;
    path << "./charts/" << title << ".png";
    plt::save(path.str());
}

void DiagnosticsEngine::plotIncrementalCharts() {
    std::vector<std::vector<int>> waits, gains, losses, numOfStarts, totalDistances;
    std::vector<string> colors, names;
    for(Color color : this->game->getBoard()->getHomeColors()) {
        waits.push_back(this->_waitCountsIncremental[color]);
        gains.push_back(this->_scoresGainedIncremental[color]);
        losses.push_back(this->_scoresLostIncremental[color]);
        numOfStarts.push_back(this->_startsCountIncremental[color]);
        totalDistances.push_back(this->_totalMoveDistanceIncremental[color]);
        colors.emplace_back(color == RED ? "r" : color == BLUE ? "b" : color == YELLOW ? "y" : "g");
        names.emplace_back(color == RED ? "Red" : color == BLUE ? "Blue" : color == YELLOW ? "Yellow" : "Green");
    }

    this->_plotCharts(this->_times, waits, "Wait Counts Incremental", names, colors, false);
    this->_plotCharts(this->_times, gains, "Scores Gained Incremental", names, colors, false);
    this->_plotCharts(this->_times, losses, "Scores Lost Incremental", names, colors, false);
    this->_plotCharts(this->_times, numOfStarts, "Starts Count Incremental", names, colors, false);
    this->_plotCharts(this->_times, totalDistances, "Distances Counts Incremental", names, colors, false);
}

void DiagnosticsEngine::plotIntervalCharts() {
    std::vector<std::vector<int>> waits, gains, losses, numOfStarts, totalDistances;
    std::vector<string> colors, names;
    for(Color color : this->game->getBoard()->getHomeColors()) {
        waits.push_back(this->_waitCountsInterval[color]);
        gains.push_back(this->_scoresGainedInterval[color]);
        losses.push_back(this->_scoresLostInterval[color]);
        numOfStarts.push_back(this->_startsCountInterval[color]);
        totalDistances.push_back(this->_totalMoveDistanceInterval[color]);
        colors.emplace_back(color == RED ? "r" : color == BLUE ? "b" : color == YELLOW ? "y" : "g");
        names.emplace_back(color == RED ? "Red" : color == BLUE ? "Blue" : color == YELLOW ? "Yellow" : "Green");
    }

    this->_plotCharts(this->_intervalTimes, waits, "Wait Counts Interval", names, colors, true);
    this->_plotCharts(this->_intervalTimes, gains, "Scores Gained Interval", names, colors, true);
    this->_plotCharts(this->_intervalTimes, losses, "Scores Lost Interval", names, colors, true);
    this->_plotCharts(this->_intervalTimes, numOfStarts, "Starts Count Interval", names, colors, true);
    this->_plotCharts(this->_intervalTimes, totalDistances, "Distances Counts Interval", names, colors, true);
}
