# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/OpenGLProjects/Mensch/AIEngine.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/AIEngine.cpp.obj"
  "D:/OpenGLProjects/Mensch/Board.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/Board.cpp.obj"
  "D:/OpenGLProjects/Mensch/BoardTile.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/BoardTile.cpp.obj"
  "D:/OpenGLProjects/Mensch/Command.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/Command.cpp.obj"
  "D:/OpenGLProjects/Mensch/CommandStream.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/CommandStream.cpp.obj"
  "D:/OpenGLProjects/Mensch/DiagnosticsEngine.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/DiagnosticsEngine.cpp.obj"
  "D:/OpenGLProjects/Mensch/Dice.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/Dice.cpp.obj"
  "D:/OpenGLProjects/Mensch/Game.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/Game.cpp.obj"
  "D:/OpenGLProjects/Mensch/GameActor.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/GameActor.cpp.obj"
  "D:/OpenGLProjects/Mensch/MoveCommand.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/MoveCommand.cpp.obj"
  "D:/OpenGLProjects/Mensch/Observer.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/Observer.cpp.obj"
  "D:/OpenGLProjects/Mensch/PhysicsEngine.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/PhysicsEngine.cpp.obj"
  "D:/OpenGLProjects/Mensch/Piece.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/Piece.cpp.obj"
  "D:/OpenGLProjects/Mensch/RenderEngine.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/RenderEngine.cpp.obj"
  "D:/OpenGLProjects/Mensch/Spawner.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/Spawner.cpp.obj"
  "D:/OpenGLProjects/Mensch/Subject.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/Subject.cpp.obj"
  "D:/OpenGLProjects/Mensch/main.cpp" "D:/OpenGLProjects/Mensch/cmake-build-debug/CMakeFiles/Mensch.dir/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
