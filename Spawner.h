#ifndef MENSCH_SPAWNER_H
#define MENSCH_SPAWNER_H

#include "Piece.h"

class Spawner {
public:
    explicit Spawner(Piece *prototype);

    Piece *spawnActor();

private:
    Piece *_prototype;
};

#endif //MENSCH_SPAWNER_H
