#include "Board.h"

Board::Board() :
        _redHome(Color::RED, TileType::HOME),
        _yellowHome(Color::YELLOW, TileType::HOME),
        _blueHome(Color::BLUE, TileType::HOME),
        _greenHome(Color::GREEN, TileType::HOME),
        _ordinaryTile(Color::NONE, TileType::ORDINARY) {

    this->_init();

};

void Board::_init() {
    this->setHomeTiles();
    this->_initTiles();
    this->_initPieces();
    this->_initHomeColors();
    this->_dice = new Dice();

}

void Board::_initTiles() {
    for (int i = 0; i < TILE_COUNT; ++i) {
        this->_tiles.push_back(nullptr);
    }

    for (int i = 0; i < TILE_COUNT; ++i) {
        if (_homeTiles.find(i) == _homeTiles.end()) {
            this->_tiles[i] = &_ordinaryTile;
        } else {
            this->_tiles[i] = _homeTiles[i];
        }
    }
}

void Board::_initPieces() {
    std::vector<Piece *> initialPieces;
    auto *redPiece = new Piece(Color::RED);
    auto *bluePiece = new Piece(Color::BLUE);
    auto *greenPiece = new Piece(Color::GREEN);
    auto *yellowPiece = new Piece(Color::YELLOW);
    initialPieces.push_back(redPiece);
    initialPieces.push_back(bluePiece);
    initialPieces.push_back(greenPiece);
    initialPieces.push_back(yellowPiece);

    for (auto it = initialPieces.begin(); it != initialPieces.end(); it++) {
        this->_pieceSpawner = new Spawner(*it);
        for (int i = 0; i < SINGLE_PIECE_NUM; ++i) {
            Piece *newPiece = this->_pieceSpawner->spawnActor();
            newPiece->setNumber(i + 1);
            this->_pieces.push_back(newPiece);
        }
    }
}

const unordered_map<int, BoardTile *> &Board::getHomeTiles() const {
    return _homeTiles;
}

void Board::setHomeTiles() {
    _homeTiles[0] = &_redHome;
    _homeTiles[10] = &_blueHome;
    _homeTiles[20] = &_greenHome;
    _homeTiles[30] = &_yellowHome;
}

Dice *Board::getDice() const {
    return _dice;
}

const vector<Piece *> &Board::getPieces() const {
    return _pieces;
}

int Board::getHomeTile(Color homeColor) {
    for (int index = 0; index < _tiles.size(); ++index) {
        if (_tiles.at(index)->getColor() == homeColor) {
            return index;
        }
    }

    return -1;
}

const vector<Color> &Board::getHomeColors() const {
    return _homeColors;
}

void Board::_initHomeColors() {
    this->_homeColors.push_back(Color::RED);
    this->_homeColors.push_back(Color::BLUE);
    this->_homeColors.push_back(Color::GREEN);
    this->_homeColors.push_back(Color::YELLOW);
}
