#ifndef MENSCH_AICOMMANDINFO_H
#define MENSCH_AICOMMANDINFO_H

#include "Command.h"
#include "Piece.h"

struct AICommandInfo {
    Command* command;
    int diceValue;
    Piece* movedPiece;
    Piece* hitPiece;
    bool isValid;

    AICommandInfo() {};
    AICommandInfo(Command* cmd, int diceVal, Piece* piece, Piece* hit, bool valid) {
        command = cmd;
        diceValue = diceVal;
        movedPiece = piece;
        hitPiece = hit;
        isValid = valid;
    }
    AICommandInfo(Command* cmd, int diceVal, Piece* piece) {
        command = cmd;
        diceValue = diceVal;
        movedPiece = piece;
    }
    AICommandInfo(Piece* piece, Piece* hit, bool valid) {
        movedPiece = piece;
        hitPiece = hit;
        isValid = valid;
    }
};

#endif //MENSCH_AICOMMANDINFO_H
