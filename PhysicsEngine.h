#ifndef MENSCH_PHYSICSENGINE_H
#define MENSCH_PHYSICSENGINE_H

#include "Subject.h"
#include "Command.h"
#include "enums/Color.h"
#include "Piece.h"

class Game;

class PhysicsEngine : public Subject {
public:
    static PhysicsEngine *getInstance();

    void updateEntity(Piece *piece);

    void checkForEnd(Game* game);

    void executeCommand(Command* command);

    void updateScores(Piece* movedPiece, Piece* hitPiece);

    Color getWinner() const;

private:
    PhysicsEngine() {};
    Color _winner;
protected:

};

#endif //MENSCH_PHYSICSENGINE_H
