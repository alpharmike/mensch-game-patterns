#include "Dice.h"
#include <random>

using namespace std;

int Dice::roll() {
    return (rand() % 6) + 1;
}
