#ifndef MENSCH_EVENT_H
#define MENSCH_EVENTTYPE_H

enum EventType {
    MOVEMENT_ALLOWED,
    STARTED,
    PIECE_HIT,
    IDLE,
    GAME_FINISHED,
    WAITED,
    SCORE_LOST,
    SCORE_GAINED,
};

#endif //MENSCH_EVENT_H
