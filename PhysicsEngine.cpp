#include "PhysicsEngine.h"
#include "enums/Color.h"
#include "Game.h"

PhysicsEngine *PhysicsEngine::getInstance() {
    static auto *instance = new PhysicsEngine();
    return instance;
}

void PhysicsEngine::updateEntity(Piece *piece) {
    if (piece != nullptr) {
        if (piece->getTotalMove() >= 0 && piece->getOldTotalMove() < 0) {
            this->subscribe(piece, STARTED);
        }
    }
}

void PhysicsEngine::executeCommand(Command *command) {
    if (command != nullptr) {
        command->execute();
    }
}

void PhysicsEngine::checkForEnd(Game *game) {
    // Notify the game if one of the piece colors have won the game
    // send GAME_FINISHED event to Game component

    for (Color pieceColor : game->getBoard()->getHomeColors()) {
        int reachedHomeRow = 0;
        for (Piece *piece : game->getBoard()->getPieces()) {
            if (piece->getColor() == pieceColor && piece->getTotalMove() >= HOME_ROW_START) {
                ++reachedHomeRow;
            }
        }

        if (reachedHomeRow == SINGLE_PIECE_NUM) {
            this->_winner = pieceColor;
            this->subscribe(game, EventType::GAME_FINISHED);
            this->notify(GAME_FINISHED);
        }
    }
}

Color PhysicsEngine::getWinner() const {
    return _winner;
}

void PhysicsEngine::updateScores(Piece *movedPiece, Piece *hitPiece) {
    if (hitPiece != nullptr) {
        this->subscribe(hitPiece, SCORE_LOST);
        this->subscribe(hitPiece, PIECE_HIT);
        this->subscribe(movedPiece, SCORE_GAINED);
    }
    if (movedPiece != nullptr && movedPiece->getTotalMove() >= HOME_ROW_START &&
               movedPiece->getOldTotalMove() < HOME_ROW_START) {
        this->subscribe(movedPiece, SCORE_GAINED);
    }
}
