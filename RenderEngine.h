#ifndef MENSCH_RENDERENGINE_H
#define MENSCH_RENDERENGINE_H

#include <unordered_map>
#include <iostream>
#include "enums/Color.h"
#include "GameActor.h"
#include "AICommandInfo.h"

class Game;

class RenderEngine {
public:
    static RenderEngine *getInstance();

    void renderGameStatus(Game* game);

    void showLatestMove(AICommandInfo commandInfo, Color turn, int round);

private:
    RenderEngine() {};

    std::unordered_map<Color, const char*> _colorTranslations;

    void _initColorTranslations();

};



#endif //MENSCH_RENDERENGINE_H
